import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/login.vue'
import Course from '../page/yuth/course.vue'
// import SoftWareEngineer from '../components/yuth/course/software_engineer.vue'
// import SoftWareTester from '../components/yuth/course/software_tester.vue'
// import PresalesEngineer from '../components/yuth/course/presales_engineer.vue'
// import HumanResource from '../components/yuth/course/human_resource.vue'
// import General from '../components/yuth/course/general.vue'
// import BusinessManagement from '../components/yuth/course/business_management.vue'
// import AccountPosition from '../components/yuth/course/account_position.vue'
// import Trainning from '../components/yuth/course/trainning.vue'
import NewsCom from '../page/bird/news_community.vue'
import News from '../components/bird/news/news.vue'
import Activity from '../components/bird/news/activity.vue'
import FeaturedNews from '../components/bird/news/feature_news.vue'
import TravelDetail from '../page/hong/travel_detail.vue'
import FunTrip from '../components/hong/traveldetail/funtrip.vue'
import Food from '../components/hong/traveldetail/food.vue'
import Shopping from '../components/hong/traveldetail/shopping.vue'
import Accommodation from '../components/hong/traveldetail/accommodation.vue'
import Knowledge from '../page/tee/knowledge.vue'
import NewKnowledge from '../components/Tee/knowledgedetail/new_knowledge.vue'
import PopularKnowledge from '../components/Tee/knowledgedetail/pop_knowledge.vue'
import KnowledgeData from '../components/Tee/knowledgedetail/knowledge_data.vue'
import Train from '../page/aoy/train.vue'
import Trainvdo from '../page/aoy/trainvdo'
import Detail from '../page/hong/detail.vue'
import knowledgePopPage from '../page/tee/knowledge_pop_page.vue'
import knowledgeNewPage from '../page/tee/knowledge_new_page.vue'
import NewsDetail from '../page/bird/detail_news_pages.vue'
import ActivityDetail from '../page/bird/detail_activity_pages.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/course',
    name: 'Course',
    component: Course
    // children: [
    //   { path: '/software-engineer', component: SoftWareEngineer },
    //   { path: '/software-tester', component: SoftWareTester },
    //   { path: '/presales-engineer', component: PresalesEngineer },
    //   { path: '/human-resource', component: HumanResource },
    //   { path: '/general', component: General },
    //   { path: '/business-management', component: BusinessManagement },
    //   { path: '/account-position', component: AccountPosition },
    //   { path: '/trainning', component: Trainning }
    // ]
  },
  {
    path: '/newscom',
    name: 'News',
    component: NewsCom,
    children: [
      { path: '/news', component: News },
      { path: '/activity', component: Activity },
      { path: '/featured-news', component: FeaturedNews }
    ]
  },
  {
    path: '/traveldetail',
    name: 'TravelDetail',
    component: TravelDetail,
    children: [
      { path: '/funtrip', component: FunTrip },
      { path: '/food', component: Food },
      { path: '/shopping', component: Shopping },
      { path: '/accommodation', component: Accommodation }
    ]
  },
  {
    path: '/knowledge',
    name: 'Knowledge',
    component: Knowledge,
    children: [
      { path: '/newknowledge', component: NewKnowledge },
      { path: '/popularknowledge', component: PopularKnowledge },
      { path: '/knowledgedata', component: KnowledgeData }
    ]
  },
  {
    path: '/train',
    name: 'Train',
    component: Train
  },
  {
    path: '/trainvdo',
    name: 'Trainvdo',
    component: Trainvdo
  },
  {
    path: '/detailTravel',
    name: 'Detail',
    component: Detail
  },
  {
    path: '/knowledge_pop_page',
    name: 'knowledgeDetailPage',
    component: knowledgePopPage
  },
  {
    path: '/knowledge_new_page',
    name: 'knowledgeNewPage',
    component: knowledgeNewPage
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/detail_news',
    name: 'NewsDetail',
    component: NewsDetail
  },
  {
    path: '/detail_activity',
    name: 'ActivityDetail',
    component: ActivityDetail
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// logout () {
//   this.$cookies.remove('datdaInfo', '/', process.env.VUE_APP_WEB_DOMAIN)
//   window.location = process.env.VUE_APP_WEB_CENTER
// }
export default router
