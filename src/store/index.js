import Vue from 'vue'
import Vuex from 'vuex'
import pathify, { make } from 'vuex-pathify'

const moduleA = {
  state: {
    menu_course_bar: 1,
    travel: [],
    newknowledge: [],
    popknowledge: [],
    train: [],
    news: [],
    activity: []
  },
  mutations: {
    set_number_menu (state, val) {
      state.menu_course_bar = val
    },
    set_travel (state, val) {
      state.travel = val
      console.log('valllll', state.travel)
    },
    set_newknowledge (state, val) {
      state.newknowledge = val
      console.log('newknowledge', state.newknowledge)
    },
    set_popknowledge (state, val) {
      state.popknowledge = val
      console.log('popknowledge', state.popknowledge)
    },
    set_train (state, val) {
      state.train = val
      console.log('train', state.train)
    },
    set_news (state, val) {
      state.news = val
      console.log('news', state.news)
    },
    set_activity (state, val) {
      state.activity = val
      console.log('activity', state.activity)
    }
  },
  actions: {
    async get_number_menu (context, val) {
      await context.commit('set_number_menu', val)
    },
    async get_travel ({ commit }, val) {
      await commit('set_travel', val)
    },
    async get_newknowledge ({ commit }, val) {
      await commit('set_newknowledge', val)
    },
    async get_popknowledge ({ commit }, val) {
      await commit('set_popknowledge', val)
    },
    async get_train ({ commit }, val) {
      await commit('set_train', val)
    },
    async get_news ({ commit }, val) {
      await commit('set_news', val)
    },
    async get_activity ({ commit }, val) {
      await commit('set_activity', val)
    }
  },
  getters: {
    travel: state => state.travel,
    newknowledge: state => state.newknowledge,
    popknowledge: state => state.popknowledge,
    train: state => state.train,
    news: state => state.news,
    activity: state => state.activity
  }
}
const state = { items: [] }
const mutations = make.mutations(state)
Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    pathify.plugin
  ],
  state,
  mutations,
  modules: {
    moduleA: moduleA
  }
})
